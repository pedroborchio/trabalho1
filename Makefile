ALL = trabalho1
all: $(ALL)

trabalho1: trabalho1.o function1.o function2.o function3.o
	gcc -o $@ $^

%.o: %.c
	gcc -c $<

cleanobj:
	rm -f *.o

cleanexe:
	rm -f $(ALL)

clean: cleanobj cleanexe
